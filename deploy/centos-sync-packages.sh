#!/bin/bash
# Source : 
# https://www.centos.org/forums/viewtopic.php?t=17429 

SOURCE=$(rpm -qa --qf "%{NAME}\n" | sort > Old.txt)
DEST=$(rpm -qa --qf "%{NAME}\n" | sort > New.txt)

echo "Put in your Source host"
read SOURCE_HOST

scp New.txt root@$DEST_HOST

echo "Put in your Destination Host"
READ DEST_HOST


ssh root@DEST 'yum install $(diff Old.txt New.txt | fgrep "<" | cut -d ' ' -f 2)'

#If added RPMs are from a 3rd part repo and have a common and unique vendor field, then the following may be useful. For instance to find all the unique vendor tags:

#Code: Select all
rpm -qa --qf "%{VENDOR}\n" | sort | uniq > Vendors.txt


#Say you find the vendor name is MyVendor, and the packages are available in a repo [myvendor], then to get a list of the packages and install them:

#Code: Select all
#rpm -qa --qf "%{NAME} %{VENDOR}\n" | sort >~/RPMS_by_Vendor.txt
#grep MyVendor RPMS_by_Vendor.txt | cut -d ' ' -f 1 > ToInstall.txt


#Then on the new system

#Code: Select all
yum --enablerepo myvendor install $(cat ToInstall.txt)

