Hosts
=====
```
[httpbin]

[srv]
srv2 srv2.example.com=192.168.4.93

[rp]
rp4 rp4.example.com=192.168.4.14

[desktop]
tobkern-desktop tobkern-desktop.example.com=192.168.4.21

```

Management 
==========
semaphore
https://github.com/ansible-semaphore/semaphore

Source:
* https://code-complete.com/code/?p=40

Docker
======
role-local-registry
* https://github.com/ansible/role-local-registry

rolling_upgrade
===============

Source:
* http://docs.ansible.com/ansible/guide_rolling_upgrade.html

Glusterfs
=========

Source :
* http://docs.ansible.com/ansible/gluster_volume_module.html
 
Installation
=============

Initiale Installation

Hier kann das install_ansible.sh script verwendet werden, dass alle notwendigen dinge einrichtet und Konfiguirt.


Allegemeine Hilfen
==================
* http://www.pro-linux.de/artikel/2/1862/2,inventar.html
* http://www.pro-linux.de/artikel/2/1862/mit-ansible-eine-wsgi-applikation-auf-entfernten-rechnern-aufsetzen.html


Ansible Tools
=============
* http://marceljuenemann.github.io/angular-drag-and-drop-lists/demo/#
* https://github.com/ansible/angular-drag-and-drop-lists
* https://github.com/ansible/angular-codemirror
* https://github.com/ansible/django-jsonbfield
* https://github.com/ansible/playbook-ansible_integration
* https://ansible-semaphore.github.io/semaphore/
* https://docs.ansible.com/ansible/playbooks_best_practices.html

Ansible API
===========
* http://docs.ansible.com/ansible/uri_module.html
* http://docs.ansible.com/ansible/dev_guide/developing_api.html
* https://liquidat.wordpress.com/2016/02/11/howto-access-red-hat-satellite-rest-api-via-ansible/
