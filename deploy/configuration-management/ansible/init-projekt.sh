#!/bin/bash

NEW-PRJ="$1"

if [ -z "${VAR}" ]; then
    echo "VAR is unset or set to the empty string"
    echo "Please user valid Name for your new Ansible Projekt now . I will exit. Please rerun if you know the new Name"
    exit 1 
fi

mkdir $NEW-PRJ

cd $NEW-PRJ

mkdir -p roles/common/{tasks,handlers,templates,files,vars,defaults,meta}
touch roles/common/{tasks,handlers,templates,files,vars,defaults,meta}/main.yml

exit 0