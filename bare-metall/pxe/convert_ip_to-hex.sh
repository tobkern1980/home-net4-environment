#!/bin/bash

[ "$#" -eq "1" ] && echo "You asked for $1 which is `ip_to_hex $1`"

IP1=`ip_to_hex 192.168.1.47`
echo "IP1 is $IP1, from 192.168.1.47"

echo "I am about to convert 10.97.2.31 into hex:"
ip_to_hex 10.97.2.31

IP_ADDRESS=172.17.3.193
echo "$IP_ADDRESS = `ip_to_hex $IP_ADDRESS`"
