#/bin/bash

# example installation with puppet 3 on cent os 7 

yum -y install https://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm

yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install https://yum.theforeman.org/releases/1.14/el7/x86_64/foreman-release.rpm

yum -y install foreman-installer


foreman-installer -v 

puppet agent --test

puppet module install puppetlabs/ntp


