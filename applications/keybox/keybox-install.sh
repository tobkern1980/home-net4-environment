#!/bin/bash
#
cd /opt
wget https://github.com/skavanagh/KeyBox/releases/download/v2.88.01/keybox-jetty-v2.88_01.tar.gz 

# source: https://wiki.ubuntuusers.de/Java/Tipps/
mv KeyBox-jetty/ keybox

sudo cp keybox.service /etc/systemd/system/

sudo chmod 754 /etc/systemd/system/keybox.service
 
sudo systemctl daemon-reload

sudo systemctl enable keybox

sudo systemctl start keybox 

exit 0