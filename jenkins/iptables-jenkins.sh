#!/bin/bash
# https://wiki.jenkins.io/display/JENKINS/Running+Jenkins+on+Port+80+or+443+using+iptables

FIRST_NIC="$1 "

sudo iptables -A PREROUTING -t nat -i ${FIRST_NIC} -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo iptables -A PREROUTING -t nat -i ${FIRST_NIC} -p tcp --dport 443 -j REDIRECT --to-port 8443

