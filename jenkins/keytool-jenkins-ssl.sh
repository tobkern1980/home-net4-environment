#!/bin/bash
#https://wiki.jenkins.io/pages/viewpage.action?pageId=135468777

DOMAIN="$1"

keytool -importkeystore -srckeystore DOMAIN.pfx -srcstoretype pkcs12 -destkeystore $DOMAIN.jks -deststoretype JKS

