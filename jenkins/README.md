## Jenkins auf Ubuntu


Version 16.04


## Verzeichnisse

/usr/share/jenkins = war datei von jenkins
/var/log/jenkins = Log von jenkins 
/var/lib/jenkins = Home dir von jenkins

## User und Gruppen


user = jenkins gruppe = jenkins

## Wichtige Plugins 
* [This plugin permits Jenkins to Deploy into containers and application servers.](https://github.com/jenkinsci/deploy-plugin)
* [Consul plugin for reading services using consul service discovery and K/V store. ](https://github.com/jenkinsci/consul-plugin)
* [GitHub Pull Request Builder Plugin](https://github.com/jenkinsci/ghprb-plugin)
* [Jenkins Git Plugin](https://github.com/jenkinsci/git-plugin)
* [Kubernetes Pipeline is Jenkins plugin which extends Jenkins Pipeline to provide native support for using Kubernetes pods, secrets and volumes to perform builds ](https://github.com/jenkinsci/kubernetes-pipeline-plugin)
* [Build Pipeline Plugin](https://github.com/jenkinsci/build-pipeline-plugin)
* [Jenkins Cloud Plugin for Docker](https://github.com/jenkinsci/docker-plugin)
* [A config-like syntax for defining Pipelines](https://github.com/jenkinsci/pipeline-model-definition-plugin)
* [Building continuous delivery pipelines and similarly complex tasks.](https://github.com/jenkinsci/pipeline-plugin)
* [Puppet module that you can include in your own tree.](https://github.com/jenkinsci/puppet-jenkins)
* [Pipeline Maven Plugin](https://github.com/jenkinsci/pipeline-maven-plugin)
* [Marathon plugin for Jenkins](https://github.com/jenkinsci/marathon-plugin)
* [Remote SSH , SFTP , Remote Shell execution , Project based team member ACL stratagy , View Table pattern based ACL stratagy](https://github.com/jenkinsci/ssh2easy-plugin)
* [Provides a means to launch slaves via SSH.](https://github.com/jenkinsci/ssh-slaves-plugin)
* [A SAML 2.0 Plugin for the Jenkins Continuous Integration server](https://github.com/jenkinsci/saml-plugin)
* [OpenShift V3 Plugin for Jenkins](https://github.com/jenkinsci/openshift-pipeline-plugin)
* [A Jenkins plugin for interfacing with GitLab](https://github.com/jenkinsci/gitlab-plugin)
* [Jenkins plugin to ship the console log off to Logstash](https://github.com/jenkinsci/logstash-plugin)
* [plunk-devops-plugin](https://github.com/jenkinsci/splunk-devops-plugin)
* [rundeck-plugin](https://github.com/jenkinsci/rundeck-plugin)
* [jenkins Vault Plugin](https://github.com/jenkinsci/hashicorp-vault-plugin)
* [openstack-cloud-plugin](https://github.com/jenkinsci/openstack-cloud-plugin)
* [Pipeline Examples](https://jenkins.io/doc/pipeline/examples/)
* [Github pipeline examples](https://github.com/jenkinsci/pipeline-examples/tree/master/pipeline-examples)
* [jenkins-pipeline-scripts](https://github.com/docker/jenkins-pipeline-scripts)
* [fabric 8 jenkins pipeline (abgewählt!)](https://github.com/fabric8io/jenkins-pipeline-library)
* [Gut hub Plugin](https://github.com/jenkinsci/github-plugin)
* [openshift-sync-plugin](https://github.com/jenkinsci/openshift-sync-plugin)
* [Foreman node Share](https://github.com/jenkinsci/foreman-node-sharing-plugin)
* [This plugin uses HashiCorp's Nomad scheduler to provision new build slaves based on workload](https://github.com/jenkinsci/nomad-plugin)
* [Mesos Plugin](https://github.com/jenkinsci/mesos-plugin)
* [prometheus-plugin](https://github.com/jenkinsci/prometheus-plugin)
* [Fluentd Plugin](https://github.com/jenkinsci/fluentd-plugin)
* [openshift-login-plugin](https://github.com/jenkinsci/openshift-login-plugin)
* [reverse-proxy-auth-plugin](https://github.com/jenkinsci/reverse-proxy-auth-plugin)
* [hello-world-plugin test](https://github.com/jenkinsci/hello-world-plugin)
* [kanboard-plugin](https://github.com/jenkinsci/kanboard-plugin)
* [kubernetes-ci-plugi](https://github.com/jenkinsci/kubernetes-ci-plugin)
* [plugin for chosing Revision / Tag before build ](https://github.com/jenkinsci/git-parameter-plugin)
* [plugin supporting dynamic label assignment ](https://github.com/jenkinsci/nodelabelparameter-plugin)
* [terraform-plugin](https://github.com/jenkinsci/terraform-plugin)
* [publishing plugin for Packer](https://github.com/jenkinsci/packer-plugin)
* [Redmine PLugin](https://github.com/jenkinsci/redmine-plugin)
* [Docker registry build plugin for Jenkins](https://github.com/jenkinsci/docker-build-publish-plugin)
* [Ansible Plugin](https://github.com/jenkinsci/ansible-plugin)
* [hadoop-plugin](https://github.com/jenkinsci/hadoop-plugin)
* [libvirt-slave-plugin](https://github.com/jenkinsci/libvirt-slave-plugin)
* [Vagrant Plugin](https://github.com/jenkinsci/vagrant-plugin)
* [Saltstack Plugin](https://github.com/jenkinsci/saltstack-plugin)

## Quellen
* [Official Jenkins Docker image](https://github.com/jenkinsci/docker)
