#!/bin/bash

sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --add-port=443/tcp --permanent
sudo firewall-cmd --add-forward-port=port=80:proto=tcp:toaddr=127.0.0.1:toport=8080 --permanent
sudo firewall-cmd --add-forward-port=port=443:proto=tcp:toaddr=127.0.0.1:toport=8443 --permanent
sudo firewall-cmd --reload
