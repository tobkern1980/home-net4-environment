#Apache-Webserver mit zwei Knoten Clustering mit Pacemaker unter CentOS 7

Pacemaker ist eine Hochverfügbarkeitsclustersoftware für Linux wie das Betriebssystem. 
Pacemaker ist als " Cluster Resource Manager " bekannt. 
Er bietet maximale Verfügbarkeit der Clusterressourcen durch Failover von Ressourcen zwischen den Clusterknoten.

Schrittmacher verwenden Corosync für den Heartbeat und die interne Kommunikation zwischen Clusterkomponenten. Corosync kümmert sich auch um Quorum in Cluster.

In diesem Artikel werden wir die Installation und Konfiguration von zwei Node Apache (httpd) Webserver-Clustering mit Pacemaker unter CentOS 7 demonstrieren.

In meinem Setup werde ich zwei virtuelle Maschinen und gemeinsam genutzten Speicher von Fedora Server verwenden (zwei Datenträger werden gemeinsam genutzt, 
wobei ein Datenträger als Fencing-Gerät und ein anderer Datenträger als gemeinsam genutzter Speicher für den Webserver verwendet wird.)


* node1.linuxtechi.com (192.168.1.51) - CentOS 7.x
* node2.linuxtechi.com (192.168.1.52) - CentOS 7.x

## Schritt: 1 Aktualisieren Sie die Datei '/etc/hosts'
Fügen Sie in beiden Knoten die folgenden Zeilen in die Datei /etc/hosts ein.

```
192.168.1.51 node1.linuxtechi.com node1
192.168.1.52 node2.linuxtechi.com node2
```
##Schritt: 2 Installieren Sie den Cluster und andere erforderliche Pakete.
Verwenden Sie den folgenden yum-Befehl auf beiden Knoten, um das Cluster-Paket (PC), die Fencing-Agents und den Webserver (httpd) zu installieren.

```
[ root @ node1 ~] # yum -y update
[ root @ node1 ~] # yum -y install pc zaun-agents-all iscsi-initiator-utils httpd
```
```
[ root @ node2 ~] # yum -y update
[ root @ node2 ~] # yum -y install pc zaun-agents-all iscsi-initiator-utils httpd
```

## Schritt: 3 Legen Sie das Kennwort für den Benutzer "hacluster" fest
Es wird empfohlen, auf beiden Knoten dasselbe Passwort des Benutzers "hacluster" zu verwenden.

```
[ root @ node1 ~] # echo <neues Kennwort> | passwd --stdin hacluster
[ root @ node2 ~] # echo <neues Kennwort> | passwd --stdin hacluster
```

## Schritt: 4 Ermöglichen Sie Hochverfügbarkeitsports in der Firewall.
Verwenden Sie den Befehl " Firewall-cmd " auf beiden Knoten, um Hochverfügbarkeits-Ports in der Betriebssystem-Firewall zu öffnen.

```
[ root @ node1 ~] # firewall-cmd --permanent --add-service = Hochverfügbarkeit
Erfolg
[ root @ node1 ~] # firewall-cmd --reload
Erfolg
[ root @ node1 ~] #

[ root @ node2 ~] # firewall-cmd --permanent --add-service = Hochverfügbarkeit
Erfolg
[ root @ node2 ~] # firewall-cmd --reload
Erfolg
[ root @ node2 ~] #
```

##Schritt: 5 Starten Sie den Clusterdienst und berechtigen Sie Knoten, dem Cluster beizutreten.
Lass uns den Clusterdienst auf beiden Knoten starten,

```
[ root @ node1 ~] # systemctl pcsd.service starten
[ root @ node1 ~] # systemctl pcsd.service aktivieren
ln -s '/usr/lib/systemd/system/pcsd.service' '/etc/systemd/system/multi-user.target.wants/pcsd.service'
[ root @ node1 ~] #

[ root @ node2 ~] # systemctl pcsd.service starten
[ root @ node2 ~] # systemctl pcsd.service aktivieren
ln -s '/usr/lib/systemd/system/pcsd.service' '/etc/systemd/system/multi-user.target.wants/pcsd.service'
[ root @ node2 ~] #
```

Verwenden Sie den folgenden Befehl auf einem der Knoten, um die Knoten für den Clusterzugriff zu autorisieren.

```
[ root @ node1 ~] # pcs cluster auth node1 node2
Benutzername: hacluster
Passwort:
node1: Autorisiert
Knoten2: Autorisiert
[ root @ node1 ~] #
```

## Schritt: 6 Erstellen Sie den Cluster und aktivieren Sie den Clusterdienst
Verwenden Sie die folgenden pcs-Befehle auf einem der Cluster-Knoten, um einen Cluster mit dem Namen ' Apachecluster ' zu erstellen , und node1 & node2 sind die Cluster-Knoten.

```
[ root @ node1 ~] # pcs cluster setup_startname Apachecluster node1 node2
Schrittmacher- / Corosync-Dienste werden heruntergefahren ...
Umleitung auf /bin/systemctl stop pacemaker.service
Weiterleitung nach /bin/systemctl stop corosync.service
Alle verbleibenden Dienste töten ...
Alle Cluster-Konfigurationsdateien entfernen ...
node1: Erfolgreich
node2: Erfolgreich
Cluster auf Knoten starten: node1, node2 ...
node2: Cluster wird gestartet ...
node1: Cluster wird gestartet ...
Pcsd-Zertifikate auf den Knoten node1, node2 ... synchronisieren
node1: Erfolg
Knoten2: Erfolg
Pcsd auf den Knoten neu laden, um die Zertifikate neu zu laden ...
node1: Erfolg
Knoten2: Erfolg
[ root @ node1 ~] #
```

Aktivieren Sie den Clusterdienst mit folgendem pcs-Befehl:

```
[ root @ node1 ~] # pcs cluster enable --all
node1: Cluster aktiviert
node2: Cluster aktiviert
[ root @ node1 ~] #
```

Überprüfen Sie jetzt den Clusterdienst

```
[ root @ node1 ~] # PC- Clusterstatus
Schrittmacher-Cluster-Service
```

##Schritt: 7 Richten Sie den iscsi shared Storage für beide Knoten auf Fedora Server ein.
IP-Adresse des Fedora 23 Servers = 192.168.1.21

Installieren Sie zuerst das erforderliche Paket.

```
[ root@fedora23 ~] # dnf -y install targetcli
Ich habe eine neue Festplatte (/dev/sdb) der Größe 11 GB auf meinem Fedora-Server, auf der ich zwei LV-Dateien für Fecing und andere für Apache-Dateisysteme erstellt habe.

[ root@fedora23 ~] # pvcreate /dev/sdb
 Physisches Volume "/dev/sdb" wurde erfolgreich erstellt
[ root@fedora23 ~] # vgcreate cluster_data /dev/sdb
 Datenträgergruppe "cluster_data" wurde erfolgreich erstellt
[ root@fedora23 ~] # lvcreate -L 1G -n fence_storage cluster_data
 Logisches Volume "fence_storage" erstellt.
[ root@fedora23 ~] # lvcreate -L 10G -n apache_storage cluster_data
 Logisches Volume "apache_storage" erstellt.
[ root@fedora23 ~] #
```

Rufen Sie die Initiator-Namen der beiden Knoten ab.

```
[ root@node1 ~] # cat /etc/iscsi/initiatorname.iscsi
InitiatorName = iqn.1994-05.com.redhat: 63ba7391df7f
[ root@node1 ~] #

[ root@node2 ~] # cat /etc/iscsi/initiatorname.iscsi
InitiatorName = iqn.1994-05.com.redhat: d4337e65531e
[ root@node2 ~] #
```

Verwenden Sie nun den Befehl ' targetcli ', um den iscsi-Speicher für beide Knoten zu konfigurieren.

```
[ root @ fedora23 ~] # targetcli
/> cd /backstores/blockieren
/backstores/block> apache-fs /dev/cluster_data/apache_storage erstellen 
/backstores/block> erstellt fence-storage /dev/cluster_data/fence_storage 
/backstores/block> cd /iscsi

/iscsi> erstellen
/iscsi> cd iqn.2003-01.org.linux-iscsi.fedora23.x8664: sn.646023b9e9c6/tpg1/luns
/iscsi/iqn.20 ... 9c6/tpg1/luns>/backstores/block/apache-fs erstellen
/iscsi/iqn.20 ... 9c6/tpg1/luns>/backstores/block/fence-storage erstellen
/iscsi/iqn.20 ... 9c6/tpg1/luns> cd ../acls
/iscsi/iqn.20 ... 9c6/tpg1/acls> Erstellen von iqn.1994-05.com.redhat: 63ba7391df7f
/iscsi/iqn.20 ... 9c6/tpg1/acls> Erstellen von iqn.1994-05.com.redhat: d4337e65531e
/iscsi/iqn.20 ... 9c6/tpg1/acls> cd /
/> saveconfig 
/> beenden
targetcli-command-fedora23
```

Starten Sie und aktivieren Sie den Zieldienst

```
[ root @ fedora23 ~] # systemctl target.service starten
[ root @ fedora23 ~] # systemctl aktiviert target.service

Öffnen Sie die iscsi-Ports in der OS-Firewall.

[ root @ fedora23 ~] # firewall-cmd --permanent --add-port = 3260 / tcp
Erfolg

[ root @ fedora23 ~] # firewall-cmd --reload
Erfolg

[ root @ fedora23 ~] #
```

Scannen Sie nun den iscsi-Speicher auf beiden Knoten:

Führen Sie die folgenden Befehle auf beiden Knoten aus

```
# iscsiadm --mode discovery --typ sendtargets --portal 192.168.1.21
# iscsiadm -m node -T iqn.2003-01.org.linux-iscsi.fedora23.x8664: sn.646023b9e9c6 -l -p 192.168.1.21:3260
Ersetzen Sie das Ziel 'iqn' und 'IP-Adresse' gemäß Ihrer Konfiguration. Nachdem Sie den obigen Befehl ausgeführt haben, können Sie in der Befehlsausgabe 'fdisk -l' zwei neue Datenträger sehen.
```

fdisk-l-Befehlsausgabe

Listen Sie die IDs der neu gescannten iscsi-Platte auf.

```
[ root @ node1 ~] # ls -l /dev/disk/by-id/
insgesamt 0
..........
lrwxrwxrwx. 1 Wurzelwurzel 9 Feb 21 03:22 wwn-0x60014056e8763c571974ec3b78812777 -> ../../sdb
lrwxrwxrwx. 1 Wurzelwurzel 9 Feb 21 03:22 wwn-0x6001405ce01173dcd7c4c0da10051405 -> ../../sdc
[ root @ node1 ~] #
```

Starten und aktivieren Sie den iscsi-Dienst auf beiden Knoten.
```
[ root @ node1 ~] # systemctl iscsi.service starten
[ root @ node1 ~] # systemctl ermöglicht iscsi.service
[ root @ node1 ~] # systemctl iscsid.service aktivieren
ln -s '/usr/lib/systemd/system/iscsid.service' '/etc/systemd/system/multi-user.target.wants/iscsid.service'
[ root @ node1 ~] #

[ root @ node2 ~] # systemctl iscsi.service starten
[ root @ node2 ~] # systemctl ermöglicht iscsi.service
[ root @ node2 ~] # systemctl iscsid.service aktivieren
ln -s '/usr/lib/systemd/system/iscsid.service' '/etc/systemd/system/multi-user.target.wants/iscsid.service'
[ root @ node2 ~] #
```

##Schritt: 8 Erstellen Sie die Clusterressourcen.
Definieren Sie das Fencing-Gerät stonith (Shoot the Other Node In The Head) für den Cluster. Es ist eine Methode, um den Knoten vom Cluster zu isolieren, wenn der Knoten nicht mehr reagiert.

Ich verwende 1 GB iscsi-Speicher ( / dev / sdc ) für das Fechten.

Führen Sie die folgenden Befehle auf einem der Knoten aus:

```
[ root @ node1 ~] # pcs stonith create scsi_fecing_device fence_scsi pcmk_host_list = "node1 node2" pcmk_monitor_action = "metadata"
[ root @ node1 ~] #
[ root @ node1 ~] # pcs stonith show
 scsi_fecing_device (stonith: fence_scsi): Knoten1 wurde gestartet
[ root @ node1 ~] #
```

Erstellen Sie jetzt eine Partition auf dem zweiten iscsi-Speicher ( /dev/sdb ), die als Dokumentenstamm für unseren Webserver verwendet wird.

```
[ root @ node1 ~] # fdisk /dev/disk/by-id/wwn-0x60014056e8763c571974ec3b78812777
create-partition-using-fdik-command
```

Formatieren Sie die neu erstellte Partition:

```
[ root @ node1 ~] # mkfs.ext4 / dev / disk / nach-id / wwn-0x60014056e8763c571974ec3b78812777-part1
Hängen Sie das neue Dateisystem temporär in / var / www ein, erstellen Sie Unterordner und legen Sie die Selinux-Regel fest.

[ root @ node1 html] # mount /dev/disk/by-id/ wwn-0x60014056e8763c571974ec3b78812777-part1/var/www/
[ root @ node1 html] # mkdir /var/www/html
[ root @ node1 html] # mkdir /var/www/cgi-bin
[ root @ node1 html] # mkdir /var/www/error
[ root @ node1 html] # restorecon -R /var/www
[ root @ node1 html] # echo "Apache-Web-Sever-Schrittmacher-Cluster"> /var/www/html/index.html
```

Umhängen Sie das Dateisystem jetzt, da das Dateisystem bei Bedarf von Cluster bereitgestellt wird.

```
[ root @ node1 html] # umount /var/www/
[ root @ node1 html] #
```

Erstellen Sie die Webserver-Dateisystem-Clusterressource mit dem Befehl pcs.

```
[ root @ node1 html] # pcs resource create webserver_fs Dateisystem device = "/dev/disk/by-id/wwn-0x60014056e8763c571974ec3b78812777-part1" verzeichnis = "/var/www" fstype = "ext4" - gruppieren Sie die Webgruppe
[ root @ node1 html] #
[ root @ node1 html] # pcs- Ressource anzeigen

 Ressourcengruppe: Webgruppe
 webserver_fs (ocf :: heartbeat: Dateisystem): Knoten1 wurde gestartet
[ root @ node1 html] #
```

Fügen Sie auf beiden Knoten die folgenden Zeilen in die Datei '/etc/httpd/conf/httpd.conf' ein.

```
<Standort / Serverstatus>
 SetHandler Server-Status
 Auftrag verweigern, zulassen
 Abgelehnt von allen
 Zulassen ab 127.0.0.1
</ Location>
```

Öffnen Sie den httpd- oder Webserver-Port in der Betriebssystemfirewall auf beiden Knoten

```
[ root @ node1 ~] # firewall-cmd --permanent --add-service = http
Erfolg
[ root @ node1 ~] # firewall-cmd --reload
Erfolg

[ root @ node1 ~] #
[ root @ node2 ~] # firewall-cmd --permanent --add-service = http
Erfolg
[ root @ node2 ~] # firewall-cmd --reload
Erfolg
[ root @ node2 ~] #
```

Erstellen Sie eine virtuelle IP-Clusterressource (IPaddr2) mit dem folgenden Befehl. Führen Sie den folgenden Befehl auf einem beliebigen Knoten aus.

```
[ root @ node1 ~] # pcs resource create vip_res IPaddr2 ip = 192.168.1.151 cidr_netmask = 24 - group webgroup
[ root @ node1 ~] #
```

Erstellen Sie eine Apache-Cluster-Ressource mit dem folgenden Befehl:

```
[ root @ node1 ~] # pcs-Ressource create apache_res apache configfile = "/etc/httpd/conf/httpd.conf" statusurl = "http://127.0.0.1/server-status" - group-webgruppe
[ root @ node1 ~] #
```

Überprüfen Sie den Clusterstatus:

```
[ root @ node1 ~] # pcs status
```
Schrittmacher-Cluster-Status

Verwenden Sie den Befehl ' df ' und ' ip add ', um das Failover des Dateisystems und der IP-Adresse zu überprüfen.

Greifen Sie mit VIP auf Ihre Website zu (192.168.1.151)

.Webserver-vip

Schrittmacher-GUI:

Die GUI von Pacemaker kann über den Webbrowser mit vip aufgerufen werden.

https://192.168.1.151:2224/

Schrittmacher-Corosync-GUI

Verwenden Sie den Benutzernamen 'hacluster' und das Kennwort, das Sie im obigen Schritt festgelegt haben.

Fügen Sie die vorhandenen Clusterknoten hinzu.

Schrittmacher-GUI-Vorhandener-Cluster

Schrittmacher-GUI-Node-Authentifizierung

Schrittmacher-GUI-Admin-Panel

Die Installation und Konfiguration des Schrittmachers ist nun abgeschlossen.

Referenz: http://clusterlabs.org/quickstart-redhat.html


Quelle:
 * [apache-clustering-using-pacemaker-on-centos-7](https://www.linuxtechi.com/two-node-apache-clustering-using-pacemaker-on-centos-7/)
 * 