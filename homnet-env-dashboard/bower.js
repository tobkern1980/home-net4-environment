{
  "name": "homenet-env",
  "description": "Dashboard for my Homenet",
  "main": "client.js",
  "authors": [
    "Tobias Kern <tobkern1980@googlemail.com>"
  ],
  "license": "ISC",
  "homepage": "http://rp4.example.com",
  "private": true,
  "ignore": [
    "**/.*",
    "node_modules",
    "bower_components",
    "test",
    "tests"
  ],
  "dependencies": {
    "angular-route": "1.6.1",
    "angular": "1.6.1",
    "google-signin": "^1.3.7",
    "google-calendar": "^1.0.3",
    "json-editor": "^0.7.28",
    "terminal-panel": "^1.0.1"
  }
}