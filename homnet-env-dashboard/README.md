# Based on  Startmin

**Startmin** is an open source, admin dashboard template for [Bootstrap](http://getbootstrap.com/) originally created by [Start Bootstrap](http://startbootstrap.com/).

You can find a demo [here](http://secondtruth.github.io/startmin).

