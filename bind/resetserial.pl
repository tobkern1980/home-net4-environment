#!/usr/bin/perl -w

die "Usage:  $0 <oldserial> <newserial>" unless (@ARGV == 2 || @ARGV == 0);

# If no arguments were specified, get them

($oldserial, $newserial) = ($ARGV[0], $ARGV[1]) if (@ARGV == 2);

if (!$oldserial) {
	print "Enter current serial number: ";
	chomp($oldserial = <STDIN>);
}
if (!$newserial) {
	print "Enter desired serial number: ";
	chomp($newserial = <STDIN>);
}

# Make sure the desired serial number is less than 2^32-1

die "You can't set a serial number to a value larger than 2^32 - 1\n" if ($newserial >= 2**32);

if ($oldserial < $newserial) {
	if ($newserial - $oldserial < 2**31) {
		print "Set serial number to ", $newserial, "\n";
	} else {
		print "Set serial number to ", $oldserial + 2**31 - 1, "\n";
		print "Set serial number to ", $newserial, "\n";
	}
} else {
	print "mod: ", ($oldserial + 2**31 - 1) % 2**32, "\n";
	if (($oldserial + 2**31 - 1) < (2**32 - 1)) {
		print "Set serial number to ", $oldserial + 2**31 - 1, "\n";
		print "Set serial number to ", $newserial, "\n";
	}  elsif ((($oldserial + 2**31 - 1) % 2**32) < $newserial) {
		print "Set serial number to ", $oldserial + 2**31 - 1, "\n";
		print "Set serial number to ", $newserial, "\n";
	} else {
		print "Set serial number to ", $newserial, "\n";
	}
}
