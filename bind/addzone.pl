#!/usr/bin/perl -w

$namedconf = "/etc/named.conf";

die "Usage: $0 <zone> <template zone data file>\n" unless (@ARGV == 2);
$zone = $ARGV[0];
$datafile = $ARGV[1];

die "$namedconf must exist" unless (-r $namedconf);

open(NAMEDCONF, ">>$namedconf") || die "Couldn't open $namedconf";

open(TEMPFILE, "$datafile") || die "Couldn't open $datafile";

open(DATAFILE, ">db.$zone") || die "Couldn't open db.$zone";
@temp = <TEMPFILE>;
print DATAFILE @temp;
close(DATAFILE);
close(TEMPFILE);

print NAMEDCONF "zone \"", $zone, "\" {\n";
print NAMEDCONF "\ttype master;\n";
print NAMEDCONF "\tfile \"", "db.$zone", "\";\n";
print NAMEDCONF "};\n";

close(NAMEDCONF);

system("rndc reconfig") && die "Couldn't reconfig name server";
