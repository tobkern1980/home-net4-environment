#!/bin/bash
# noch nicht geprüft 27.06.2017 22:05:16 
set -o nounset                              # Treat unset variables as an error

__ScriptVersion="2017.06.27"
__ScriptName="backup_foreman.sh"

# Backup script truth values
BS_TRUE=1
BS_FALSE=0

# Default sleep time used when waiting for daemons to start, restart and checking for these running
__DEFAULT_SLEEP=3

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  __detect_color_support
#   DESCRIPTION:  Try to detect color support.
#----------------------------------------------------------------------------------------------------------------------
_COLORS=${BS_COLORS:-$(tput colors 2>/dev/null || echo 0)}
__detect_color_support() {
    if [ $? -eq 0 ] && [ "$_COLORS" -gt 2 ]; then
        RC="\033[1;31m"
        GC="\033[1;32m"
        BC="\033[1;34m"
        YC="\033[1;33m"
        EC="\033[0m"
    else
        RC=""
        GC=""
        BC=""
        YC=""
        EC=""
    fi
}
__detect_color_support

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  echoerr
#   DESCRIPTION:  Echo errors to stderr.
#----------------------------------------------------------------------------------------------------------------------
__usage() {
    cat << EOT
  Usage :  ${__ScriptName} [options] <install-type> 
  
    Installation types:
    - backup     Backup Postgresql, SQLite ans Puppet Master. This is the default
    - resotre    Restores Postgresql, SQLite ans Puppet Master to a specific Host.
    
    Examples:
    - ${__ScriptName} 
    - ${__ScriptName} restore

EOT
}   # ----------  end of function __usage  ----------

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  echoerr
#   DESCRIPTION:  Echo errors to stderr.
#----------------------------------------------------------------------------------------------------------------------
echoerror() {
    printf "${RC} * ERROR${EC}: %s\n" "$@" 1>&2;
}

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  echoinfo
#   DESCRIPTION:  Echo information to stdout.
#----------------------------------------------------------------------------------------------------------------------
echoinfo() {
    printf "${GC} *  INFO${EC}: %s\n" "$@";
}

#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  echowarn
#   DESCRIPTION:  Echo warning informations to stdout.
#----------------------------------------------------------------------------------------------------------------------
echowarn() {
    printf "${YC} *  WARN${EC}: %s\n" "$@";
}


#---  FUNCTION  -------------------------------------------------------------------------------------------------------
#          NAME:  __check_command_exists
#   DESCRIPTION:  Check if a command exists.
#----------------------------------------------------------------------------------------------------------------------
__check_command_exists() {
    command -v "$1" > /dev/null 2>&1
}


while getops ':Ppsah' opt
do 
  case "${opt}" in
  
   P ) _BACKUP_PGSQL 	;;
   p ) _BACKUP_PUPPET 	;;
   s ) _BCAKUP_SQLITE 	;;
   a ) BACKUP		;;
   h )  __usage; exit 0                                ;;
   \?) echo 
         echoerror "Option does not exist : $OPTARG"
         __usage
         exit 1
         ;;
  esac    # --- end of case ---
done

ME=`basename $0` 
DATE=$(date '+%Y%m%d.%H%M')
BACKUPDIR=/data/backups/backup-$DATE
mkdir $BACKUPDIR
chgrp postgres $BACKUPDIR
chmod g+w $BACKUPDIR

BACKUP () {
	cd $BACKUPDIR
	# Backup postgres database
	foreman-rake db:dump > /usr/share/foreman/db/foreman-backup.log
	
	# Backup config ifles
	tar --selinux -czf $BACKUPDIR/etc_foreman_dir.tar.gz /etc/foreman
	tar --selinux -czf $BACKUPDIR/var_lib_puppet_dir.tar.gz /var/lib/puppet/ssl
	tar --selinux -czf $BACKUPDIR/tftpboot-dhcp.tar.gz /var/lib/tftpboot /etc/dhcp/ /var/lib/dhcpd/
	
	ls -lh *tar.gz foreman.dump
	
	cd -
}

BACKUP 2>&1 | /usr/bin/logger -t $ME
GET_DB_BACKUP="$(printf "%s"$(cat /usr/share/foreman/db/foreman-backup.sql | cut -d: -f 2| head -n 1))"
DESTINATION="/usr/share/foreman/db${GET_DB_BACKUP})"

export $DESTINATION

RECOVERY () {
	cd $BACKUPDIR
	# Restore postgres database
	foreman-rake db:import_dump file=$DESTINATION
	
	tar -xzvf $BACKUPDIR/etc_foreman_dir.tar.gz -C /
	tar --selinux -xzvf $BACKUPDIR/var_lib_puppet_dir.tar.gz -C /

}

